import { configureStore } from "@reduxjs/toolkit";
import mapReducer from "./slices/map";

const reducer = {
  map: mapReducer,
};

const store = configureStore({
  reducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ["map/setOlContext", "map/setOlMap"],
        ignoredPaths: ["map"],
      },
    }),
  devTools: true,
});

export default store;
