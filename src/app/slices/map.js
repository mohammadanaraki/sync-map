import { createSlice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

const initialState = {
  olContext: null,
  mbContext: null,
  center: [59.5179861, 36.3213941],
  zoom: 4,
  pitch: 0,
  rotation: 0,
  provider: null,
  lineStrings: [],
};

const mapSlice = createSlice({
  name: "map",
  initialState,
  reducers: {
    setOlContext: (state, action) => {
      state.olContext = action.payload;
    },
    setMbContext: (state, action) => {
      state.mbContext = action.payload;
    },
    setCenter: (state, action) => {
      state.center = action.payload.center;
      state.provider = action.payload.provider;
    },
    setZoom: (state, action) => {
      state.zoom = action.payload.zoom;
      state.provider = action.payload.provider;
    },
    setProvider: (state, action) => {
      state.provider = action.payload;
    },
    setProperty: (state, action) => {
      state.zoom = action.payload.zoom;
      state.center = action.payload.center;
      state.rotation = action.payload.rotation;
      state.provider = action.payload.provider;
    },
    addLineString: (state, action) => {
      state.lineStrings.push(action.payload);
    },
    removeLineString: (state, action) => {
      const id = action.payload;
      state.lineStrings = state.lineStrings.filter((lineString) => {
        return lineString.id !== id;
      });
    },
    removeLineStrings: (state, action) => {
      const ids = action.payload.map((item) => item.id);
      state.lineStrings = state.lineStrings.filter((lineString) => {
        return !ids.includes(lineString.id);
      });
    },
    updateLineString: (state, action) => {
      const id = action.payload.id;
      const lineString = action.payload.feature;
      state.lineStrings = state.lineStrings.map((item) => {
        if (item.id === id) {
          return lineString;
        }
        return item;
      });
    },
    updateLineStrings: (state, action) => {
      const features = action.payload;
      state.lineStrings = state.lineStrings.map((item) => {
        const index = features.findIndex((feature) => feature.id === item.id);
        if (index !== -1) {
          return features[index];
        }
        return item;
      });
    },
  },
});

export const {
  setCenter,
  setMbContext,
  setOlContext,
  setZoom,
  setProvider,
  setProperty,
  addLineString,
  removeLineString,
  updateLineString,
  removeLineStrings,
  updateLineStrings,
} = mapSlice.actions;
export default mapSlice.reducer;
