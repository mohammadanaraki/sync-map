import React from "react";
import OLMap from "./openlayers/map/map";
import MBMap from "./mapbox/map/map";
import "./App.css";
import LineString from "./openlayers/layer/LineString";
import DragBoxAndSelect from "./openlayers/layer/DragBoxAndSelect";

const App = () => {
  return (
    <div className="container">
      <OLMap>
        <LineString />
        <DragBoxAndSelect />
      </OLMap>
      <MBMap />
    </div>
  );
};

export default App;
