import React, { useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import mapboxgl from "!mapbox-gl"; // eslint-disable-line import/no-webpack-loader-syntax
import MapboxDraw from "@mapbox/mapbox-gl-draw";
import {
  addLineString,
  removeLineStrings,
  setProperty,
  setProvider,
  updateLineStrings,
} from "../../app/slices/map";
import "./map.css";
import "mapbox-gl/dist/mapbox-gl.css";
import "@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css";
mapboxgl.accessToken =
  "pk.eyJ1IjoibW9oYW1tYWRhbmFyYWtpIiwiYSI6ImNrd21nYmJkZzJibzUycG1qc2lha243NjUifQ.aEcCUP0PICzq3SlJ6Wy_xQ";

const MapComponent = ({ children }) => {
  const dispatch = useDispatch();
  const { lineStrings, provider, center, zoom, rotation } = useSelector(
    (state) => state.map
  );
  const block = provider === null ? false : provider !== "mapbox";

  const mapContainer = useRef(null);
  const map = useRef(null);
  const mapDraw = useRef(null);

  useEffect(() => {
    if (map.current) return;
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/streets-v11",
      center,
      zoom: zoom - 1,
      // disble mapbox pitch coz i cant find pitch in ol map
      pitchWithRotate: false,
    });
    mapDraw.current = new MapboxDraw({
      displayControlsDefault: false,
      controls: {
        line_string: true,
        trash: true,
      },
    });

    map.current.on("draw.create", function (e) {
      const feature = e.features[0];
      dispatch(addLineString(feature));
    });

    map.current.on("draw.delete", function (e) {
      dispatch(removeLineStrings(e.features));
    });

    map.current.on("draw.update", function (e) {
      dispatch(updateLineStrings(e.features));
    });

    map.current.on("moveend", () => {
      dispatch(setProvider(null));
    });

    map.current.on("move", () => {
      if (block) return;
      dispatch(
        setProperty({
          center: [map.current.getCenter().lng, map.current.getCenter().lat],
          zoom: map.current.getZoom() + 1,
          rotation: map.current.getBearing(),

          provider: "mapbox",
        })
      );
    });

    map.current.addControl(mapDraw.current);

    return () => {
      map.current.un("draw.create");
      map.current.un("draw.delete");
      map.current.un("draw.update");
      map.current.un("moveend");
      map.current.un("move");
      map.current.remove();
    };
  }, []);

  useEffect(() => {
    if (!map.current) return;
    if (!block) return;

    map.current.jumpTo({
      center,
      zoom: zoom - 1,
      bearing: rotation,
    });
  }, [center, zoom]);
  useEffect(() => {
    if (!mapDraw.current) return;
    if (!map.current) return;

    mapDraw.current.deleteAll();

    for (let i = 0; i < lineStrings.length; i++) {
      const element = lineStrings[i];
      if (!mapDraw.current.get(element.id)) {
        mapDraw.current.add(element);
      }
    }
  }, [lineStrings]);
  return (
    <div ref={mapContainer} className="mb-map">
      {children}
    </div>
  );
};
export default MapComponent;
