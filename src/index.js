import React from "react";
import ReactDOM from "react-dom";
import "./styles/index.css";
import store from "./app/store";
import { Provider } from "react-redux";

import App from "./App";

const Wrapper = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

ReactDOM.render(<Wrapper />, document.getElementById("root"));
