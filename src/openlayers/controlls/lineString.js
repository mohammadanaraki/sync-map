import { Control } from "ol/control";
import Draw from "ol/interaction/Draw";

import "./index.css";

class LineStringControl extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const button = document.createElement("button");
    button.className = "draw_linestring";
    const element = document.createElement("div");
    element.className = "linestring ol-unselectable ol-control";
    element.appendChild(button);

    super({
      element: element,
      target: options.target,
    });

    button.addEventListener("click", this.handleRotateNorth.bind(this), false);
  }

  handleRotateNorth() {
    this.getMap()
      .getInteractions()
      .forEach((interaction) => {
        if (interaction instanceof Draw) {
          interaction.setActive(true);
        }
      });
  }
}

export default LineStringControl;
