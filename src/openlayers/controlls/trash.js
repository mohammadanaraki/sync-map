import { Control } from "ol/control";
import Draw from "ol/interaction/Draw";
import "./index.css";
import { Select } from "ol/interaction";

class TrashControl extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const button = document.createElement("button");
    button.className = "draw_trash";
    const element = document.createElement("div");
    element.className = "trash ol-unselectable ol-control";
    element.appendChild(button);

    super({
      element: element,
      target: options.target,
    });

    button.addEventListener("click", this.handleRotateNorth.bind(this), false);
  }

  handleRotateNorth() {
    let vectorSource = null;
    this.getMap()
      .getLayers()
      .forEach((layer) => {
        if (layer.get("name") === "LineString") {
          vectorSource = layer.getSource();
        }
      });

    const features = [];

    this.getMap()
      .getInteractions()
      .forEach((interaction) => {
        if (interaction instanceof Select) {
          interaction.getFeatures().forEach((feature) => {
            vectorSource.removeFeature(feature, { fast: true });

            features.push(feature);
          });
        }
      });
    vectorSource.dispatchEvent({
      type: "removefeature",
      target: vectorSource,
      features,
    });
    // features.forEach((feature) => {
    //   console.log("trash ", feature.getId());
    //   vectorSource.removeFeature(feature);
    // });
  }
}

export default TrashControl;
