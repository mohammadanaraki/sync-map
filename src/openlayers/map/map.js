import React, { useRef, useEffect } from "react";
import "ol/ol.css";

import "./map.css";
import { useSelector, useDispatch } from "react-redux";
import { setOlContext, setProperty } from "../../app/slices/map";
import Map from "ol/Map";
import View from "ol/View";

import { OSM } from "ol/source";
import { Tile as TileLayer } from "ol/layer";
import { fromLonLat, toLonLat } from "ol/proj";
import TrashControl from "../controlls/trash";
import LineStringControl from "../controlls/lineString";

import { Control, defaults as defaultControls } from "ol/control";
const MapComponent = ({ children }) => {
  const dispatch = useDispatch();

  const map = useSelector((state) => state.map.olContext);
  const { center, zoom, rotation, provider } = useSelector(
    (state) => state.map
  );

  const mapRef = useRef();

  const block = provider === null ? false : provider !== "ol";
  useEffect(() => {
    let mapObject = new Map({
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      controls: defaultControls().extend([
        new TrashControl(),
        new LineStringControl(),
      ]),
      overlays: [],
      view: new View({
        center: fromLonLat(center),
        zoom,
      }),
    });

    mapObject.setTarget(mapRef.current);
    dispatch(setOlContext(mapObject));

    return () => mapObject.setTarget(undefined);
  }, []);

  useEffect(() => {
    if (!map) return;

    const changeFunc = (e) => {
      let center = toLonLat(map.getView().getCenter());
      let zoom = map.getView().getZoom();
      let rotate = map.getView().getRotation();

      dispatch(
        setProperty({
          center,
          zoom,
          rotation: (rotate * -180) / Math.PI,
          provider: "ol",
        })
      );
    };

    if (!block) {
      map.getView().on("propertychange", changeFunc);
    }

    return () => {
      if (!block) {
        map.getView().un("propertychange", changeFunc);
      }
    };
  }, [block, map]);

  useEffect(() => {
    if (!map) return;
    if (!block) return;

    map.getView().setCenter(fromLonLat(center));
    map.getView().setZoom(zoom);
    map.getView().setRotation((rotation / -180) * Math.PI);
  }, [center, zoom, rotation]);

  return (
    <div ref={mapRef} className="ol-map">
      {children}
    </div>
  );
};
export default MapComponent;
