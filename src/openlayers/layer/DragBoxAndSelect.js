import React, { useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { DragBox, Select } from "ol/interaction";
import { platformModifierKeyOnly } from "ol/events/condition";
import { v4 as uuidv4 } from "uuid";
import Feature from "ol/Feature";
import GeoJSON from "ol/format/GeoJSON";
import { removeLineString, removeLineStrings } from "../../app/slices/map";

const DragBoxAndSelectComponent = ({ children }) => {
  const dispatch = useDispatch();

  const { provider } = useSelector((state) => state.map);

  const block = provider === null ? false : provider !== "ol";

  const map = useSelector((state) => state.map.olContext);

  useEffect(() => {
    if (!map) return;

    // https://openlayers.org/en/latest/examples/select-dragbox.html
    // https://openlayers.org/en/latest/apidoc/module-ol_interaction_Select.html
    // https://openlayers.org/en/latest/apidoc/module-ol_interaction_DragBox.html

    let vectorSource = null;
    map.getLayers().forEach((layer) => {
      if (layer.get("name") === "LineString") {
        vectorSource = layer.getSource();
      }
    });

    // a normal select interaction to handle click
    const select = new Select();
    map.addInteraction(select);

    const selectedFeatures = select.getFeatures();

    // a DragBox interaction used to select features by drawing boxes
    const dragBox = new DragBox({
      condition: platformModifierKeyOnly,
    });

    map.addInteraction(dragBox);

    dragBox.on("boxend", function () {
      // features that intersect the box geometry are added to the
      // collection of selected features

      // if the view is not obliquely rotated the box geometry and
      // its extent are equalivalent so intersecting features can
      // be added directly to the collection
      const rotation = map.getView().getRotation();
      const oblique = rotation % (Math.PI / 2) !== 0;
      const candidateFeatures = oblique ? [] : selectedFeatures;
      const extent = dragBox.getGeometry().getExtent();
      vectorSource.forEachFeatureIntersectingExtent(extent, function (feature) {
        candidateFeatures.push(feature);
      });

      // when the view is obliquely rotated the box extent will
      // exceed its geometry so both the box and the candidate
      // feature geometries are rotated around a common anchor
      // to confirm that, with the box geometry aligned with its
      // extent, the geometries intersect

      if (oblique) {
        const anchor = [0, 0];
        const geometry = dragBox.getGeometry().clone();
        geometry.rotate(-rotation, anchor);
        const extent = geometry.getExtent();
        candidateFeatures.forEach(function (feature) {
          const geometry = feature.getGeometry().clone();
          geometry.rotate(-rotation, anchor);
          if (geometry.intersectsExtent(extent)) {
            selectedFeatures.push(feature);
          }
        });
      }
    });

    // clear selection when drawing a new box and when clicking on the map
    dragBox.on("boxstart", function () {
      selectedFeatures.clear();
    });

    return () => {
      map.removeInteraction(dragBox);
      map.removeInteraction(select);
    };
  }, [map]);

  useEffect(() => {
    if (!map) return;
    let vectorSource = null;
    map.getLayers().forEach((layer) => {
      if (layer.get("name") === "LineString") {
        vectorSource = layer.getSource();
      }
    });
    // event listener for when features are changed
    vectorSource.on("removefeature", function (evt) {
      const features = evt.features;

      if (features) {
        const serlializedFeatures = features.map((feature) => {
          const geojson = new GeoJSON();
          const featureGeoJson = geojson.writeFeatureObject(feature, {
            featureProjection: "EPSG:3857",
          });
          return featureGeoJson;
        });
        dispatch(removeLineStrings(serlializedFeatures));
      }
    });
  }, [map]);

  return <div>{children}</div>;
};
export default DragBoxAndSelectComponent;
