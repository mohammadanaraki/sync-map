import React, { useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addLineString } from "../../app/slices/map";

import Draw from "ol/interaction/Draw";
import { Vector as VectorSource } from "ol/source";
import { Vector as VectorLayer } from "ol/layer";
import { fromLonLat, toLonLat } from "ol/proj";
import GeoJSON from "ol/format/GeoJSON";
import Feature from "ol/Feature";
import LineString from "ol/geom/LineString";

import { v4 as uuidv4 } from "uuid";

const LineStringComponent = ({ children }) => {
  const dispatch = useDispatch();

  const map = useSelector((state) => state.map.olContext);
  const lineStrings = useSelector((state) => state.map.lineStrings);

  const source = useRef(null);

  useEffect(() => {
    if (!map) return;
    source.current = new VectorSource();

    const r = new VectorSource();
    const vector = new VectorLayer({
      source: source.current,
    });

    vector.set("name", "LineString");

    const draw = new Draw({
      source: source.current,
      type: "LineString",
      stopClick: true,
    });
    draw.setActive(false);
    draw.on("drawend", (e) => {
      const id = uuidv4();
      const feature = e.feature;
      feature.setId(id);

      var writer = new GeoJSON();
      var geoJsonStr = writer.writeFeatureObject(feature, {
        featureProjection: "EPSG:3857",
      });
      dispatch(addLineString(geoJsonStr));
      draw.setActive(false);
    });
    map.addInteraction(draw);
    map.addLayer(vector);

    return () => {
      draw.un("drawend");
      map.removeInteraction(draw);
    };
  }, [map]);

  useEffect(() => {
    if (!map) return;
    if (!source.current) return;
    source.current.clear({ fast: true });
    for (let i = 0; i < lineStrings.length; i++) {
      const element = lineStrings[i];
      if (!source.current.getFeatureById(element.id)) {
        const feature = new Feature({
          geometry: new LineString(
            element.geometry.coordinates.map((coordinate) => {
              return fromLonLat(coordinate);
            })
          ),
        });
        feature.setId(element.id);
        source.current.addFeature(feature);
      } else {
        const feature = source.current.getFeatureById(element.id);
        feature.setGeometry(
          new LineString(
            element.geometry.coordinates.map((coordinate) => {
              return fromLonLat(coordinate);
            })
          )
        );
      }
    }
  }, [lineStrings, map]);
  return <div>{children}</div>;
};
export default LineStringComponent;
